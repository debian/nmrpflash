#!/bin/sh

set -e
set -u

BRANCH=upstream/master
PKG=nmrpflash
VER=$(git describe --always $BRANCH|sed 's/^v//')
ORIG_TAR="../${PKG}_${VER}.orig.tar.gz"
git archive --format=tar "--prefix=$PKG-$VER/" "$BRANCH" \
    | gzip -9 -n > "$ORIG_TAR"

echo "$ORIG_TAR ready."
